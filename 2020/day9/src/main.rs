use std::fs::File;
use std::io::prelude::*;

fn validate(numbers : &Vec<u64>, preamble : usize) -> (usize, u64) {
    for (idx, n) in numbers.iter().enumerate() {
        if idx < preamble {
            continue;
        }
        let preamble = &numbers[idx-preamble..idx];
        let mut found = false;
        for p1 in preamble {
            for p2 in preamble {
                if p1 + p2 == *n {
                    found = true;
                    break;
                }
            }
            if found {
                break;
            }
        }
        if !found {
            println!("invalid number {:?} at index {}", n, idx);
            return (idx, *n);
        }
    }
    panic!("weak spot not found");
}

fn find_key(numbers : &Vec<u64>, weaknum : u64) -> u64 {
    for count in 2..numbers.len() { // try various lengths of keys
        for idx  in 0..(numbers.len()-count) { //this key length starting at this position
            let buff = &numbers[idx..idx+count];
            let total : u64 = buff.iter().sum();
            if total == weaknum {
                println!("key found {:?}", buff);
                let min = buff.iter().min().unwrap();
                let max = buff.iter().max().unwrap();
                return min + max;
            }
        }
    }
    0
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines = contents.split("\n");
    let mut numbers : Vec<u64> = lines.map(|l| l.parse().unwrap()).collect::<Vec<u64>>();
    //println!("numbers {:?}", numbers);
    let (weakidx, weaknum) : (usize, u64) = validate(&numbers, 25);
    println!("weakidx {:?} weaknum {}", weakidx, weaknum);
    numbers.retain(|x| *x < weaknum);
    let key = find_key(&numbers, weaknum);
    println!("key {:?}", key);
    Ok(())
}
