use std::fs::File;
use std::io::prelude::*;

#[derive(Debug)]
struct Map {
    walkable : Vec<Vec<char>>,
}

impl Map {
    fn from(contents : &String) -> Map {
        let lines = contents.split("\n");
        let mut walkable = Vec::new();
        for l in lines {
            //let linechars : Vec<bool> = l.chars().map(|chr| chr == '.').collect();
            let linechars : Vec<char> = l.chars().collect();
            walkable.push(linechars);
        }
        Map {
            walkable
        }
    }

    fn walkable(&mut self, x : usize, y : usize) -> bool {
        let modx = x % self.width();
        let retr = self.walkable[y][modx] == '.';
        if retr {
            self.walkable[y][modx] = 'O';
        } else {
            self.walkable[y][modx] = 'X';
        }
        retr
    }

    fn print(&self) {
        for y in 0..self.height() {
            for x in 0..self.width() {
                print!("{}", self.walkable[y][x]);
            }
            println!("");
        }
        println!("");
    }

    fn height(&self) -> usize {
        self.walkable.len()
    }

    fn width(&self) -> usize {
        self.walkable[0].len()
    }
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let slopes = vec![(1,1), (3,1), (5,1), (7,1), (1,2)];
    let mut slope_trees : u64 = 1;
    for (dx, dy) in slopes {
        let mut m = Map::from(&contents);
        let mut x = 0;
        let mut y = 0;
        let mut trees = 0;
        while y < m.height()-1 {
            x += dx;
            y += dy;
            if m.walkable(x, y) == false {
                trees += 1;
            }
        }
        println!("slope ({},{}), trees {:?}", dx, dy, trees);
        slope_trees *= trees;
    }
    println!("slope_trees {:?}", slope_trees);
    Ok(())
}