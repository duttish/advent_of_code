use std::iter::FromIterator;
use std::fs::File;
use std::io::prelude::*;

use std::collections::HashSet;
use std::collections::HashMap;

use regex::Regex;
#[macro_use] extern crate lazy_static;

#[derive(Debug, Clone)]
struct Field {
    name: String,
    ranges : Vec<(usize, usize)>
}

impl Field {
    fn parse(text : &str) -> Field {
        lazy_static! {
            static ref RX : Regex = Regex::new(r"(?P<name>.+): (?P<amin>\d+)-(?P<amax>\d+) or (?P<bmin>\d+)-(?P<bmax>\d+)").unwrap();
        }
        if let Some(captures) = RX.captures(text) {
            let name = captures.name("name").unwrap().as_str();
            let amin = captures.name("amin").unwrap().as_str().parse::<usize>().unwrap();
            let amax = captures.name("amax").unwrap().as_str().parse::<usize>().unwrap();
            let bmin = captures.name("bmin").unwrap().as_str().parse::<usize>().unwrap();
            let bmax = captures.name("bmax").unwrap().as_str().parse::<usize>().unwrap();
            return Field {
                name: name.to_string(),
                ranges : vec![(amin, amax), (bmin, bmax)]
            }
        }
        panic!("failed to parse {:?}", text);
    }
    fn validate(&self, numbers : &Vec<usize>) -> HashSet<usize> {
        let mut invalids = HashSet::new();
        for &n in numbers {
            let mut found = false;
            for r in &self.ranges {
                if r.0 <= n && r.1 >= n {
                    found = true;
                }
            }
            if !found {
                invalids.insert(n);
            }
        }
        invalids
    }

    fn validate_num(&self, num : usize) -> bool {
        for r in &self.ranges {
            if r.0 <= num && num <= r.1 {
                return true
            }
        }
        false
    }
}

fn part1(ranges : &Vec<Field>, nearby : Vec<Vec<usize>>) {
    let mut all_invalids = Vec::new();
    for nticket in &nearby {
        let mut ticket_invalids : HashSet<usize> = HashSet::from_iter(nticket.clone().into_iter());
        for r in ranges {
            let invalidnums = r.validate(nticket);
            ticket_invalids = ticket_invalids.intersection(&invalidnums).cloned().collect();
        }
        if ticket_invalids.is_empty() {
            continue;
        }
        ticket_invalids.into_iter().for_each(|ti| all_invalids.push(ti));
    }
    println!("all_invalids {:?}, sum {}", all_invalids, all_invalids.iter().sum::<usize>());
}

fn part2(ranges : &Vec<Field>, your : &Vec<usize>, nearby : &Vec<Vec<usize>>) {
    let mut valid = Vec::new();
    for nticket in nearby {
        let mut ticket_invalids : HashSet<usize> = HashSet::from_iter(nticket.clone().into_iter());
        for r in ranges {
            let invalidnums = r.validate(nticket);
            ticket_invalids = ticket_invalids.intersection(&invalidnums).cloned().collect();
        }
        if ticket_invalids.is_empty() {
            valid.push(nticket);
            continue;
        }
    }

    let mut options : HashMap<usize, Vec<Field>> = HashMap::new();
    // all fields can have all options
    for idx in 0..your.len() {
        options.insert(idx, ranges.to_vec());
    }
    let mut known : Vec<(usize, Field)> = Vec::new();
    //list all valid options per field
    for v in valid {
        for fidx in 0..v.len() {
            let mut franges = options.get(&fidx).unwrap().clone();
            let mut to_remove = Vec::new();
            for r in &franges {
                if !r.validate_num(v[fidx]) {
                    to_remove.push(r.clone());
                }
            }
            for tr in to_remove {
                franges.retain(|fr| fr.name != tr.name);
            }
            if franges.len() == 1 { //only remaining possiblity
                known.push((fidx, franges[0].clone()));
            }
            options.insert(fidx, franges);
        }
    }
    //now let's eliminate
    let mut visited = Vec::new();
    while known.len() > 0 {
        let (kidx, kdata) = known.pop().unwrap();
        if visited.contains(&kidx) {
            continue;
        }
        visited.push(kidx);
        for fidx in 0..your.len() {
            if kidx == fidx {
                continue;
            }
            if let Some(alts) = options.get_mut(&fidx) {
                alts.retain(|f| f.name != kdata.name);
                if alts.len() == 1 {
                    known.push((fidx, alts[0].clone()))
                }
            }
        }
    }
    for idx in 0..your.len() {
        assert_eq!(options.get(&idx).unwrap().len(), 1);
    }

    let mut total = 1;
    for ridx in 0..ranges.len() {
        let field = &options.get(&ridx).unwrap()[0];
        if field.name.contains("departure ") {
            println!("your[{}] = {}", field.name, your[ridx]);
            total *= your[ridx];
        }
    }
    println!("total product {:?}", total);
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let rx = Regex::new(r"\A(?s)(.+)\n\nyour ticket:\n(.+)\n\nnearby tickets:\n(.+)\z").unwrap();
    let (ranges, your, nearby) = match rx.captures(&contents) {
        Some(captures) => {
            let ranges = captures.get(1).unwrap().as_str();
            let ranges = ranges.split("\n").map(|r| Field::parse(r) ).collect::<Vec<Field>>();
            let your = captures.get(2).unwrap().as_str();
            let your = your.split(",").map(|t| t.parse::<usize>().unwrap()).collect::<Vec<usize>>();
            let nearby_whole = captures.get(3).unwrap().as_str();
            let nearby : Vec<Vec<usize>> = nearby_whole.split("\n").map(|line| line.split(",").map(|c| c.parse().unwrap()).collect()).collect();
            (ranges, your, nearby)
        },
        None => panic!("failed to parse {:?}", contents)
    };
    part2(&ranges, &your, &nearby);
    Ok(())
}
