use std::fs::File;
use std::io::prelude::*;
use std::collections::HashMap;

fn get_nth_p1(numbers : &Vec<u32>, index : usize) -> u32 {
    let mut retr = numbers.clone();
    while retr.len() < index {
        let last = retr[retr.len()-1];
        let mut prev_indexes = Vec::new();
        for (idx, num) in retr.iter().rev().enumerate() {
            if *num == last {
                prev_indexes.push(idx);
                if prev_indexes.len() >= 2 {
                    break;
                }
            }
        }
        if prev_indexes.len() == 1 {
            retr.push(0);
        } else if prev_indexes.len() == 2 {
            let diff = prev_indexes[1] - prev_indexes[0];
            retr.push(diff as u32);
        } else {
            panic!("prev_indexes weird {:?}", prev_indexes);
        }
    }
    retr[retr.len()-1]
}

fn get_nth_p2(numbers : &Vec<u32>, index : usize) -> u32 {
    let mut count = numbers.len();
    let mut last = numbers[numbers.len()-1];
    let mut last_seen : HashMap<u32, u32> = HashMap::new();
    for (idx, num) in numbers[0..numbers.len()-1].iter().enumerate() {
        last_seen.insert(*num, idx as u32);
    }
    while count < index {
        let mut prev_indexes = vec![count as u32 -1];
        if let Some(last_seen_index) = last_seen.get_mut(&last) {
            prev_indexes.push(*last_seen_index);
        }
        last_seen.insert(last, count as u32 - 1);
        count += 1;
        if prev_indexes.len() == 1 {
            last = 0;
        } else if prev_indexes.len() == 2 {
            let diff = prev_indexes[0] - prev_indexes[1];
            last = diff as u32;
        } else {
            panic!("prev_indexes weird {:?}", prev_indexes);
        }
    }
    last
}

fn get_nth(numbers : &Vec<u32>, index : usize) -> u32 {
    get_nth_p2(numbers, index)
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let numbers = contents.split(",").map(|l| l.parse::<u32>().unwrap()).collect::<Vec<u32>>();
    let val = get_nth(&numbers, 30000000);
    println!("val {:?}", val);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn name036() {
        let numbers = vec![0,3,6];
        assert_eq!(get_nth(&numbers, 2020), 436);
    }

    #[test]
    fn name132() {
        let numbers = vec![1,3,2];
        assert_eq!(get_nth(&numbers, 2020), 1);
    }

    #[test]
    fn name213() {
        let numbers = vec![2,1,3];
        assert_eq!(get_nth(&numbers, 2020), 10);
    }

    #[test]
    fn name123() {
        let numbers = vec![1,2,3];
        assert_eq!(get_nth(&numbers, 2020), 27);
    }

    #[test]
    fn name231() {
        let numbers = vec![2,3,1];
        assert_eq!(get_nth(&numbers, 2020), 78);
    }

    #[test]
    fn name321() {
        let numbers = vec![3,2,1];
        assert_eq!(get_nth(&numbers, 2020), 438);
    }

    #[test]
    fn name312() {
        let numbers = vec![3,1,2];
        assert_eq!(get_nth(&numbers, 2020), 1836);
    }

    #[test]
    fn name6() {
        let numbers = vec![9,3,1,0,8,4];
        assert_eq!(get_nth(&numbers, 7), 0);
        assert_eq!(get_nth(&numbers, 8), 3);
        assert_eq!(get_nth(&numbers, 9), 6);
        assert_eq!(get_nth(&numbers, 10), 0);
        assert_eq!(get_nth(&numbers, 11), 3);
    }

    //#[test]
    //fn name036_3000etc() {
    //    let numbers = vec![0,3,6];
    //    assert_eq!(get_nth(&numbers, 30000000), 175594);
    //}
    //
    //#[test]
    //fn name132_3000etc() {
    //    let numbers = vec![1,3,2];
    //    assert_eq!(get_nth(&numbers, 30000000), 2578);
    //}
    //
    //#[test]
    //fn name213_3000etc() {
    //    let numbers = vec![2,1,3];
    //    assert_eq!(get_nth(&numbers, 30000000), 3544142);
    //}
    //
    //#[test]
    //fn name123_3000etc() {
    //    let numbers = vec![1,2,3];
    //    assert_eq!(get_nth(&numbers, 30000000), 261214);
    //}
    //
    //#[test]
    //fn name231_3000etc() {
    //    let numbers = vec![2,3,1];
    //    assert_eq!(get_nth(&numbers, 30000000), 6895259);
    //}
    //
    //#[test]
    //fn name321_3000etc() {
    //    let numbers = vec![3,2,1];
    //    assert_eq!(get_nth(&numbers, 30000000), 18);
    //}
    //
    //#[test]
    //fn name312_3000etc() {
    //    let numbers = vec![3,1,2];
    //    assert_eq!(get_nth(&numbers, 30000000), 362);
    //}
}
