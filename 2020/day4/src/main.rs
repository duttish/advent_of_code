use std::collections::HashMap;

use std::fs::File;
use std::io::prelude::*;

use regex::Regex;

#[derive(Debug)]
struct Passport {
    values : HashMap<String, String>
}

impl Passport {
    fn from(text : &String) -> Passport {
        let parts = text.split(" ");
        let mut values = HashMap::new();
        for p in parts {
            let pair = p.split(":").collect::<Vec<&str>>();
            if pair.len() != 2 {
                println!("p {:?}", p);
                println!("pair {:?}", pair);
                panic!("text {:?}", text);
            }
            values.insert(pair[0].to_string(), pair[1].to_string());
        }
        Passport {
            values
        }
    }
    fn validate_1(&self) -> bool {
        let required = vec![
            "byr",
            "iyr",
            "eyr",
            "hgt",
            "hcl",
            "ecl",
            "pid",
        ];
        for r in required {
            if !self.values.contains_key(r) {
                //println!("{:?} missing {:?}", self.values, r);
                return false;
            }
        }
        true
    }

    fn validate_year(&self, field : &str, min : i32, max : i32) -> bool {
        let text = self.values.get(field).unwrap();
        let re = Regex::new(r"\d{4}").unwrap();
        if !re.is_match(text) {
            //println!("{} {:?} not four digits", field, text);
            return false;
        }
        let value = text.parse::<i32>().unwrap();
        if value < min || value > max {
            //println!("{} {:?} not in range", field, value);
            return false;
        } else {
            //println!("valid {} {:?}", field, byr);
        }
        true
    }

    fn validate_pattern(&self, name : &str, pattern : &str) -> bool {
        let text = self.values.get(name).unwrap();
        let re = Regex::new(pattern).unwrap();
        if !re.is_match(text) {
            //println!("{} {:?} not matching pattern {}", name, text, pattern);
            return false;
        }
        true
    }

    fn validate_2(&self) -> bool {
        if !self.validate_1() {
            return false;
        }

        //if let Some(cid) = self.values.get("cid") {
        //    println!("cid {:?}", cid);
        //}

        if !self.validate_year("byr", 1920, 2002) {
            return false;
        }

        if !self.validate_year("iyr", 2010, 2020) {
            return false;
        }

        if !self.validate_year("eyr", 2020, 2030) {
            return false;
        }

        //hgt
        let hgt_text = self.values.get("hgt").unwrap();
        let re = Regex::new(r"((?P<metric>\d+)cm)|((?P<squirrel>\d+)in)").unwrap();
        if !re.is_match(hgt_text) {
            //println!("hgt {:?} not matching pattern", hgt_text);
            return false;
        }
        let captures = re.captures(hgt_text).unwrap();
        if let Some(c) = captures.name("metric") {
            //println!("metric c {:?}", c.as_str());
            let hgt = c.as_str().parse::<i32>().unwrap();
            if hgt < 150 || hgt > 193 {
                //println!("metric hgt {:?} not in range", hgt);
                return false;
            } else {
                //println!("valid metric {:?}", c.as_str());
            }
        } else if let Some(c) = captures.name("squirrel") {
            //println!("squirrel c {:?}", c.as_str());
            let hgt = c.as_str().parse::<i32>().unwrap();
            if hgt < 59 || hgt > 76 {
                //println!("squirrel hgt {:?} not in range", hgt);
                return false;
            } else {
                //println!("valid squirrel {:?}", c.as_str());
            }
        }

        //hcl
        if !self.validate_pattern("hcl", r"#[0-9a-f]{6}") {
            return false;
        }

        //ecl
        let ecl_text = self.values.get("ecl").unwrap();
        let valid = vec!["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
        if valid.contains(&ecl_text.as_str()) == false {
            //println!("ecl {:?} not allowed value", ecl_text);
            return false;
        } else {
            //println!("valid ecl {:?}", ecl_text);
        }

        //pid
        if !self.validate_pattern("pid", r"^\d{9}$") {
            return false;
        }

        true
    }
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let parts = contents.split("\n\n");
    let clean_parts = parts.map(|p| p.replace("\n", " ").trim().to_string()).collect::<Vec<String>>();
    let passports = clean_parts.iter().map(|cp| Passport::from(cp)).collect::<Vec<Passport>>();
    let mut valid = 0;
    for p in passports {
        if p.validate_2() {
            valid += 1;
            println!("{:?}", p.values);
        }
    }
    println!("valid {:?}", valid);
    Ok(())
}