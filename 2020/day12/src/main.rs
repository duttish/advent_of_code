use std::fs::File;
use std::io::prelude::*;

use std::ops::Mul;

#[derive(Debug)]
enum Instruction {
    North(i32),
    South(i32),
    East(i32),
    West(i32),
    Right(i32),
    Left(i32),
    Forward(i32),
}

impl Instruction {
    fn parse(text : &String) -> Instruction {
        let chr = text.chars().next().unwrap();
        let num = text[1..].parse::<i32>().unwrap();
        match chr {
            'N' => Instruction::North(num),
            'S' => Instruction::South(num),
            'E' => Instruction::East(num),
            'W' => Instruction::West(num),
            'R' => Instruction::Right(num),
            'L' => Instruction::Left(num),
            'F' => Instruction::Forward(num),
            _ => panic!("unknown instruction {:?}", chr),
        }
    }
}

#[derive(Debug)]
struct WpVec {
    x : i32,
    y : i32
}

impl WpVec {
    fn from(x : i32, y : i32) -> WpVec {
        WpVec {
            x,
            y
        }
    }

    fn rotate(&mut self, degrees : f32) {
        let radians = degrees * std::f32::consts::PI / 180.0;
        let fx = self.x as f32;
        let fy = self.y as f32;
        let newx = fx * radians.cos() - fy * radians.sin();
        let newy = fx * radians.sin() + fy * radians.cos();
        self.x = newx.round() as i32;
        self.y = newy.round() as i32;
    }

    fn mul(&mut self, dist : i32) -> WpVec {
        WpVec {
            x: self.x * dist,
            y: self.y * dist,
        }
    }
}


fn apply_part1(i : &Instruction, x : i32, y : i32, dir : i32) -> (i32, i32, i32) {
    let mut retrx = x;
    let mut retry = y;
    let mut retrdir = dir;
    match *i {
        Instruction::North(dist) => retry -= dist,
        Instruction::South(dist) => retry += dist,
        Instruction::East(dist) => retrx -= dist,
        Instruction::West(dist) => retrx += dist,
        Instruction::Right(degrees) => {
            assert!(degrees % 90 == 0);
            assert!(degrees > 0);
            let steps = degrees / 90;
            retrdir = (dir + steps) % 4;
        },
        Instruction::Left(degrees) => {
            assert!(degrees % 90 == 0);
            assert!(degrees > 0);
            let steps = degrees / 90;
            retrdir = (dir-steps).rem_euclid(4);
        },
        Instruction::Forward(dist) => {
            match dir {
                0 => retrx -= dist, // east
                2 => retrx += dist, // west
                3 => retry -= dist, //north
                1 => retry += dist, //south
                _ => panic!("unknown direction {:?}", dir)
            }
        }
    }
    (retrx, retry, retrdir)
}

fn apply_part2(i : &Instruction, x : i32, y : i32, wp : &mut WpVec) -> (i32, i32) {
    let mut retrx = x;
    let mut retry = y;
    match *i {
        Instruction::North(dist) => wp.y += dist,
        Instruction::South(dist) => wp.y -= dist,
        Instruction::East(dist) => wp.x += dist,
        Instruction::West(dist) => wp.x -= dist,
        Instruction::Right(degrees) => {
            assert!(degrees % 90 == 0);
            assert!(degrees > 0);
            wp.rotate(-degrees as f32);
        },
        Instruction::Left(degrees) => {
            assert!(degrees % 90 == 0);
            assert!(degrees > 0);
            wp.rotate(degrees as f32);
        },
        Instruction::Forward(dist) => {
            let data = wp.mul(dist);
            retrx = x + data.x;
            retry = y + data.y;
        }
    }
    (retrx, retry)
}

fn main() -> std::io::Result<()> {
    let mut v = WpVec::from(1, 0);
    v.rotate(90.0);

    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines = contents.split("\n").map(|l| String::from(l)).collect::<Vec<String>>();
    let instructions = lines.iter().map(|l| Instruction::parse(l)).collect::<Vec<Instruction>>();
    println!("{:?}", instructions);

    let mut x = 0i32;
    let mut y = 0i32;
    let mut dir = 0;

    let mut wp = WpVec::from(10, 1);

    for i in instructions {
        //println!("i {:?}", i);

        //part 1
        //let retr = apply_part1(&i, x, y, dir);
        //x = retr.0;
        //y = retr.1;
        //dir = retr.2;
        //println!("x {} y {} dir {}", x, y, dir);

        //part 2
        // y positive is now north, fyi
        let retr = apply_part2(&i, x, y, &mut wp);
        x = retr.0;
        y = retr.1;
        //println!("x {} y {} wp {:?}", x, y, wp);
    }
    println!("manhattan dist {:?}", x.abs() + y.abs());
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn vec_rotate_left() {
        let mut v = WpVec::from(2, 1);
        v.rotate(90.0);
        assert_eq!(v.x, -1);
        assert_eq!(v.y, 2);
        v.rotate(90.0);
        assert_eq!(v.x, -2);
        assert_eq!(v.y, -1);
        v.rotate(90.0);
        assert_eq!(v.x, 1);
        assert_eq!(v.y, -2);
        v.rotate(90.0);
        assert_eq!(v.x, 2);
        assert_eq!(v.y, 1);
    }

    #[test]
    fn vec_rotate_right() {
        let mut v = WpVec::from(2, 1);
        v.rotate(-90.0);
        assert_eq!(v.x, 1);
        assert_eq!(v.y, -2);
        v.rotate(-90.0);
        assert_eq!(v.x, -2);
        assert_eq!(v.y, -1);
        v.rotate(-90.0);
        assert_eq!(v.x, -1);
        assert_eq!(v.y, 2);
        v.rotate(-90.0);
        assert_eq!(v.x, 2);
        assert_eq!(v.y, 1);
    }

    #[test]
    fn vec_rotate_about() {
        let mut v = WpVec::from(2, -1);
        v.rotate(180.0);
        assert_eq!(v.x, -2);
        assert_eq!(v.y, 1);
        v.rotate(180.0);
        assert_eq!(v.x, 2);
        assert_eq!(v.y, -1);
    }

    #[test]
    fn vec_rotate_about_neg() {
        let mut v = WpVec::from(2, -1);
        v.rotate(-180.0);
        assert_eq!(v.x, -2);
        assert_eq!(v.y, 1);
        v.rotate(-180.0);
        assert_eq!(v.x, 2);
        assert_eq!(v.y, -1);
    }
}