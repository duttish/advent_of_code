use std::fs::File;
use std::io::prelude::*;

use std::collections::HashMap;

use regex::Regex;

fn parse_child(text : &str) -> (usize, String) {
    let re = Regex::new(r"^(\d+) (.+?) bags?").unwrap();
    let m = re.captures(text).unwrap();
    let num = m.get(1).unwrap().as_str().parse::<usize>().unwrap();
    let colour = m.get(2).unwrap().as_str();
    (num, colour.to_string())
}

fn get_parent_count(name : &str, child_to_parent : &HashMap<String, Vec<String>>) -> usize {
    if !child_to_parent.contains_key(name) {
        return 0;
    }
    let mut to_visit : Vec<String> = child_to_parent.get(name).unwrap().clone();
    let mut visited = Vec::new();
    let mut count = 0;
    while to_visit.len() > 0 {
        //println!("to_visit {:?}", to_visit);
        let parent = to_visit.pop().unwrap();
        //println!("parent {:?}", parent);
        if visited.contains(&parent) {
            continue;
        }
        visited.push(parent.clone());
        count += 1;
        match child_to_parent.get(&parent) {
            Some(parents_parents) => {
                for pp in parents_parents {
                    to_visit.push(pp.clone())
                }
            },
            None => {}
        }
        //let mut parents_parents = .unwrap().clone();
    }
    count
}

fn get_child_count(name : &str, parent_to_child : &HashMap<String, Vec<(usize, String)>>) -> usize {
    let mut to_visit : Vec<(usize, String)> = parent_to_child.get(name).unwrap().clone();
    let mut visited = Vec::new();
    let mut total_count = 0;
    while to_visit.len() > 0 {
        println!("to_visit {:?}", to_visit);
        let (count, parent) = to_visit.pop().unwrap();
        println!("count {} parent {:?}", count, parent);
        //if visited.contains(&parent) {
        //    continue;
        //}
        visited.push(parent.clone());
        println!("{} + {} = {}", total_count, count, total_count + count);
        total_count += count;
        match parent_to_child.get(&parent) {
            Some(parents_parents) => {
                for (ppc, pp) in parents_parents {
                    to_visit.push((ppc * count, pp.clone()))
                }
            },
            None => {}
        }
    }
    total_count
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines = contents.split("\n");
    let mut parent_to_child : HashMap<String, Vec<(usize, String)>> = HashMap::new();
    for l in lines {
        //println!("l {:?}", l);
        let parts : Vec<String> = l.split(" contain ").map(|s| String::from(s)).collect();
        let parent = &parts[0];
        let parent = parent.strip_suffix(" bags").unwrap();
        if parts[1] == "no other bags." { // no children
            parent_to_child.insert(String::from(parent), Vec::new());
        } else if parts[1].contains(",") { // multiple children
            let children = parts[1].split(", ");
            let children : Vec<(usize, String)> = children.map(|c| parse_child(c)).collect();
            assert_eq!(parent_to_child.insert(String::from(parent), children), None);
        } else { // one child
            let child = parse_child(&parts[1]);
            assert_eq!(parent_to_child.insert(String::from(parent), vec![child]), None);
        }
    }
    let mut child_to_parent : HashMap<String, Vec<String>> = HashMap::new();
    for (parent, children) in &parent_to_child {
        for (_count, child) in children {
            match child_to_parent.get_mut(child) {
                Some(parents) => {
                    parents.push(parent.clone());
                },
                None => {
                    child_to_parent.insert(child.clone(), vec![parent.clone()]);
                }
            }
        }
    }
    //println!("parent_to_child {:?}", parent_to_child);
    //println!("child_to_parent {:?}", child_to_parent);
    let parent_count = get_parent_count("shiny gold", &child_to_parent);
    let child_count = get_child_count("shiny gold", &parent_to_child);
    println!("total parent count {:?}", parent_count);
    println!("total child count {}", child_count);
    Ok(())
}

