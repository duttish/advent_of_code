use std::fs::File;
use std::io::prelude::*;

#[derive(Debug)]
struct Seat {
    row: u16,
    col: u16,
    seat_id : u16,
}

impl Seat {
    fn from(line : &str) -> Seat {
        let rowtext = &line[0..7];
        let rowbintext = rowtext.chars().map(|c| if c == 'B' { 1 } else { 0 }).collect::<Vec<u16>>();
        let mut row = 0;
        rowbintext.iter().enumerate().for_each(|(idx, val)| row |= val <<(6-idx));

        let coltext = &line[7..10];
        let colbintext = coltext.chars().map(|c| if c == 'R' { 1 } else { 0 }).collect::<Vec<u16>>();
        let mut col = 0;
        colbintext.iter().enumerate().for_each(|(idx, val)| col |= val <<(2-idx));

        Seat {
            row : row,
            col : col,
            seat_id : row*8 + col,
        }
    }
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines = contents.split("\n");
    let seats = lines.map(|l| Seat::from(l)).collect::<Vec<Seat>>();
    let mut max = 0;
    for s in &seats {
        if s.seat_id > max {
            max = s.seat_id;
        }
    }
    println!("max seat id {:?}", max);

    let seat_ids = seats.iter().map(|s| s.seat_id ).collect::<Vec<u16>>();
    for i in 0..max {
        if !seat_ids.contains(&i) {
            println!("missing {:?}", i);
        }
    }
    Ok(())
}
