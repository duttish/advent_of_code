use std::fs::File;
use std::io::prelude::*;

use regex::Regex;

#[derive(Debug)]
struct Data {
    low : usize,
    high : usize,
    chr : char,
    psw : String,
}

impl Data {
    pub fn from(text : &str) -> Data {
        //println!("parsing {:?}", text);
        let re = Regex::new(r"^(\d+)-(\d+) (\D): (\D+)$").unwrap();
        let m = re.captures(text).unwrap();
        let low = m.get(1).unwrap().as_str().parse::<usize>().unwrap();
        let high = m.get(2).unwrap().as_str().parse::<usize>().unwrap();
        let chr = m.get(3).unwrap().as_str().chars().next().unwrap();
        let psw = m.get(4).unwrap().as_str().to_string();
        //println!("low {:?}, high {:?}, chr {:?}, psw {:?}", low, high, chr, psw);
        Data {
            low,
            high,
            chr,
            psw
        }
    }
}

fn is_valid_part1(data : &Data) -> bool {
    let matches : Vec<&str> = data.psw.matches(data.chr).collect();
    let count = matches.len();
    //println!("{:?} has {} matches", data.psw, count);
    if data.low > count {
        false
    } else if count > data.high {
        false
    } else {
        true
    }
}

fn is_valid_part2(data : &Data) -> bool {
    let chars : Vec<char> = data.psw.chars().collect();
    (chars[data.low-1] == data.chr) ^ (chars[data.high-1] == data.chr)
}

fn main() -> std::io::Result<()> {
    println!("reading file");
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines = contents.split("\n");
    let mut data : Vec<Data> = Vec::new();
    println!("parsing data");
    for l in lines {
        data.push(Data::from(&l));
    }
    println!("validating");
    let mut valid_count = 0;
    for d in &data {
        println!("{:?}", d);
        if is_valid_part2(d) {
            println!("{:?} is valid", d);
            valid_count += 1;
        }
    }
    println!("{} valid passwords", valid_count);
    Ok(())
}