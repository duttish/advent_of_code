use std::fs::File;
use std::io::prelude::*;
use std::collections::HashMap;

use regex::Regex;
#[macro_use] extern crate lazy_static;

#[derive(Debug)]
enum Instruction {
    Nop(i32),
    Acc(i32),
    Jmp(i32)
}

impl Instruction {
    fn parse_args(captures : regex::Captures) -> i32 {
        let sign = captures.get(1).unwrap().as_str();
        let diff = captures.get(2).unwrap().as_str().parse::<i32>().unwrap();
        if sign == "+" {
            diff
        } else {
            -diff
        }
    }
    pub fn parse(text : &str) -> Instruction {
        lazy_static! {
            static ref NOP_RX : Regex = Regex::new(r"nop (\+|-)(.+)$").unwrap();
            static ref ACC_RX : Regex = Regex::new(r"acc (\+|-)(.+)$").unwrap();
            static ref JMP_RX : Regex = Regex::new(r"jmp (\+|-)(.+)$").unwrap();
        }
        if let Some(captures) = NOP_RX.captures(text) {
            Instruction::Nop(Instruction::parse_args(captures))
        } else if let Some(captures) = ACC_RX.captures(text) {
            Instruction::Acc(Instruction::parse_args(captures))
        } else if let Some(captures) = JMP_RX.captures(text) {
            Instruction::Jmp(Instruction::parse_args(captures))
        } else {
            panic!("Can't parse {:?}", text);
        }
    }
}

#[derive(Debug)]
struct VmState {
    pub ip : i32,
    pub acc : i32,
}

impl VmState {
    pub fn new() -> VmState {
        VmState {
            ip : 0,
            acc : 0,
        }
    }
}

fn step(instr : &Instruction, state : &mut VmState) {
    match instr {
        Instruction::Nop(_) => {
            state.ip += 1;
        },
        Instruction::Acc(count) => {
            state.ip += 1;
            state.acc += count;
        },
        Instruction::Jmp(count) => {
            state.ip += count.clone();
        }
    }
}

// build a line -> [parent line 1, parent line 2, ...] map
fn build_flowgraph(instructions : &Vec<Instruction>) -> HashMap<usize, Vec<usize>> {
    let mut flowmap : HashMap<usize, Vec<usize>> = HashMap::new();
    for idx in 0..instructions.len() {
        match instructions[idx] {
            Instruction::Acc(_) | Instruction::Nop(_) => {
                let target = idx+1; //next line has this line as parent
                match flowmap.get_mut(&target) {
                    Some(parents) => { parents.push(idx); },
                    None => { flowmap.insert(target, vec![idx]); }
                };
            },
            Instruction::Jmp(diff) => {
                let target : usize = (idx as i32 + diff) as usize; //target line has this line as parent
                match flowmap.get_mut(&target) {
                    Some(parents) => { parents.push(idx); },
                    None => { flowmap.insert(target, vec![idx]); }
                };

            }
        }
    }
    flowmap
}

fn mark_winning(instructions : &Vec<Instruction>, graph : HashMap<usize, Vec<usize>>) -> HashMap<usize, bool> {
    let mut winning = HashMap::new();
    let mut to_check = Vec::new();
    //let curr = instructions.len();
    for idx in (0..instructions.len()).rev() { // mark initial winning
        println!("checking {:?}", idx);
        match instructions[idx] {
            Instruction::Acc(_) | Instruction::Nop(_) => {
                println!("marking {:?} as winning", idx);
                winning.insert(idx, true);
                for parent in graph.get(&idx).unwrap() {
                    to_check.push(parent);
                    println!("marking parent {:?} as winning", parent);
                    winning.insert(*parent, true);
                }
            },
            Instruction::Jmp(diff) => {
                if diff > 0 {
                    println!("marking {:?} as winning", idx);
                    winning.insert(idx, true);
                    for parent in graph.get(&idx).unwrap() {
                        to_check.push(parent);
                        println!("marking parent {:?} as winning", parent);
                        winning.insert(*parent, true);
                    }
                } else {
                    break;
                }
            }
        };
    };
    let mut visited = Vec::new();
    while !to_check.is_empty() {
        let instr_idx = to_check.pop().unwrap();
        if visited.contains(instr_idx) {
            continue;
        }
        visited.push(*instr_idx);
        println!("marking parents of {:?} as winning", instr_idx);
        if let Some(parents) = graph.get(&instr_idx) {
            for parent in parents {
                winning.insert(*parent, true);
                to_check.push(parent);
            }
        }
    }

    for idx in 0..instructions.len() {
        if winning.contains_key(&idx) {
            continue;
        }
        winning.insert(idx, false);
    }
    winning
}

fn would_swap_win(instr : &Instruction, eip : i32, winning : &HashMap<usize, bool>) -> bool {
    match instr {
        Instruction::Nop(diff) => {
            let target = (eip + diff) as usize;
            winning.get(&target).unwrap().clone()
        },
        Instruction::Acc(_) => {
            false
        },
        Instruction::Jmp(_) => {
            let target = eip as usize + 1;
            winning.get(&target).unwrap().clone()
        }
    }
}

fn run_swapping(instructions : &Vec<Instruction>, winning : &HashMap<usize, bool>) -> VmState {
    let mut state = VmState::new();
    let mut visited_ips = Vec::new();
    while (state.ip as usize) < instructions.len() {
        let instruction = &instructions[state.ip as usize];
        if would_swap_win(instruction, state.ip, winning) {
            panic!("try swap at {:?}", state.ip);
        }
        if visited_ips.contains(&state.ip) {
            panic!("loop, would again run {:?} at {:?}", instruction, state);
        }
        visited_ips.push(state.ip);
        println!("i {:?} at {}", instruction, state.ip);
        step(instruction, &mut state);
    }
    state
}

fn run(instructions : &Vec<Instruction>) -> VmState {
    let mut state = VmState::new();
    let mut visited_ips = Vec::new();
    while (state.ip as usize) < instructions.len() {
        let instruction = &instructions[state.ip as usize];
        if visited_ips.contains(&state.ip) {
            panic!("loop, would again run {:?} at {:?}", instruction, state);
        }
        visited_ips.push(state.ip);
        println!("i {:?} at {}", instruction, state.ip);
        step(instruction, &mut state);
    }
    state
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input.fixed")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines = contents.split("\n");
    let instructions = lines.map(|l| Instruction::parse(l)).collect::<Vec<Instruction>>();
    //for i in &instructions {
    //    println!("i {:?}", i);
    //}
    let endstate = run(&instructions);
    let graph = build_flowgraph(&instructions);
    let winning = mark_winning(&instructions, graph);
    //println!("winning {:?}", winning);
    //run_swapping(&instructions, &winning);
    //for (child, parents) in graph {
    //    println!("{:?} => {}", parents, child);
    //}
    println!("endstate {:?}", endstate);
    Ok(())
}
