use std::fs::File;
use std::io::prelude::*;

fn part1(contents : &String) {
    let mut lines = contents.split("\n");
    let arrive = lines.next().unwrap().parse::<usize>().unwrap();
    let timestamps = lines.next().unwrap().split(",").collect::<Vec<&str>>();
    let timestamps = timestamps.iter().filter(|c| **c != "x").cloned().map(|d| d.parse::<usize>().unwrap()).collect::<Vec<usize>>();
    println!("arrive {:?}", arrive);
    println!("timestamps {:?}", timestamps);
    let timestamps = timestamps.into_iter().map(|t| { let mut nt = t; while nt < arrive { nt += t;} (t, nt) } ).collect::<Vec<(usize, usize)>>();
    println!("timestamps {:?}", timestamps);
    let first_time = timestamps.iter().map(|t| t.1).min().unwrap();
    println!("first_time {:?}", first_time);
    let first_ts = timestamps.iter().filter(|t| t.1 == first_time).cloned().collect::<Vec<(usize, usize)>>()[0];
    println!("first_ts {:?}", first_ts);
    let diff = first_ts.1 - arrive;
    let val = diff * first_ts.0;
    println!("val {:?}", val);
}
fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    part1(&contents);
    Ok(())
}
