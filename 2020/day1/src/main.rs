use std::fs::File;
use std::io::prelude::*;


fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines = contents.split("\n");
    let numbers : Vec<u32> = lines.map(|text| text.parse().unwrap() ).collect();
    let mut below = Vec::new();
    let mut above = Vec::new();
    for n in numbers {
        if n < 1000 {
            below.push(n);
        } else {
            above.push(n);
        }
    }
    for a in &above {
        for b in &below {
            if a + b == 2020 {
                println!("match {} * {} = {}", a, b, a * b);
            }
        }
    }

    for a in &above {
        for b in &below {
            for b2 in &below {
                if a + b + b2 == 2020 {
                    println!("match {} * {} * {} = {}", a, b, b2, a * b * b2);
                }
            }
        }
    }
    //println!("above {:?}\nbelow {:?}", above, below);
    Ok(())
}