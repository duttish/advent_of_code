use std::fs::File;
use std::io::prelude::*;
use std::collections::HashSet;

fn get_group_count(group : Vec<String>) -> usize {
    let mut common_answers : HashSet<char> = HashSet::from(group[0].chars().collect());
    for person in &group[1..] {
        let mut person_answers : HashSet<char> = HashSet::new();
        for answer in person.chars() {
            person_answers.insert(answer);
        }
        common_answers = common_answers.intersection(&person_answers).cloned().collect::<HashSet<char>>();
    }
    common_answers.len()
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let groups = contents.split("\n\n");
    let mut total_answers = 0;
    for g in groups {
        let people = g.split("\n").map(|t| String::from(t)).collect::<Vec<String>>();
        let group_count = get_group_count(people);
        total_answers += group_count;
    }
    println!("total_answers {:?}", total_answers);
    Ok(())
}
