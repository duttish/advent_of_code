use std::fs::File;
use std::io::prelude::*;
use std::fmt;
use std::collections::HashMap;

use regex::Regex;
#[macro_use] extern crate lazy_static;

enum Opv1 {
    Write {
        address : usize,
        value : u64
    },
    Mask {
        // this mask will be used to set the 1 bits
        // so we OR with this one
        onemask : u64,
        // ... and the 0 bits, through AND
        zeromask : u64,
    }
}

impl Opv1 {
    fn empty_mask() -> Opv1 {
        Opv1::Mask { onemask : 0, zeromask: 0 }
    }
    fn apply(&self, value : u64) -> u64 {
        match self {
            Opv1::Mask { onemask, zeromask} => {
                let mut retr = value;
                retr |= onemask;
                retr &= zeromask;
                retr
            }
            Opv1::Write { address: _, value: _} => panic!("apply on a value {:?}", self),
        }
    }
    fn parse(text : &str) -> Opv1 {
        lazy_static! {
            static ref MASK_RX : Regex = Regex::new(r"mask = (.+)$").unwrap();
            static ref WRITE_RX : Regex = Regex::new(r"mem\[(\d+)\] = (\d+)$").unwrap();
        }
        if let Some(captures) = MASK_RX.captures(text) {
            let mask = captures.get(1).unwrap().as_str();
            println!("mask {:?}", mask);
            let mut onemask = 0;
            let mut zeromask = 0;
            for (idx, chr) in mask.chars().rev().enumerate() {
                if chr == 'X' {
                    zeromask |= 1 << idx;
                    continue;
                } else if chr == '1' {
                    onemask |= 1 << idx;
                    zeromask |= 1 << idx;
                } else if chr == '0' {
                    zeromask &= !(1 << idx);
                }
            }
            Opv1::Mask { onemask, zeromask }
        } else if let Some(captures) = WRITE_RX.captures(text) {
            let address = captures.get(1).unwrap().as_str().parse::<usize>().unwrap();
            let value = captures.get(2).unwrap().as_str().parse::<u64>().unwrap();
            println!("write {} = {}", address, value);
            Opv1::Write {
                address,
                value
            }
        } else {
            panic!("can't parse {:?}", text);
        }
    }
}

impl fmt::Debug for Opv1 {
    fn fmt(&self, f : &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Opv1::Mask { onemask, zeromask } => write!(f, "mask: onemask {:b} zeromask {:b}", onemask, zeromask),
            Opv1::Write { address, value } => write!(f, "write[{}] = {}", address, value),
        }
    }
}

fn part1(contents : &String) {
    let lines = contents.split("\n");
    let ops = lines.map(|l| Opv1::parse(l)).collect::<Vec<Opv1>>();
    println!("ops {:?}", ops);
    let mut current_mask = Opv1::empty_mask();
    let mut memory = Vec::new();
    for op in ops {
        match op {
            Opv1::Mask { onemask: _, zeromask: _ } => current_mask = op,
            Opv1::Write { address, value } => {
                if address >= memory.len() {
                    memory.resize(address + 1, 0);
                }
                let val = current_mask.apply(value);
                println!("writing mem[{}] = {}", address, val);
                memory[address as usize] = val;
            }
        }
    }
    let nonzero : u64 = memory.into_iter().filter(|&v| v != 0).sum();
    println!("nonzero {:?}", nonzero);
}

enum Opv2 {
    Write {
        address : usize,
        value : u64
    },
    Mask {
        //
        xes : String,
        // this mask will be used to set the 1 bits
        // so we OR with this one
        onemask : u64,
    }
}

fn set_char_at(text : &String, index : usize, chr : char) -> String {
    let mut retr = String::new();
    for (idx, c) in text.chars().enumerate() {
        if idx == index {
            retr.push(chr);
        } else {
            retr.push(c);
        }
    }
    retr
}

impl Opv2 {
    fn empty_mask() -> Opv2 {
        Opv2::Mask { onemask : 0, xes : String::new() }
    }
    fn apply(&self, value : u64) -> Vec<u64> {
        match self {
            Opv2::Mask { onemask, xes } => {
                let mut masked_value = value;
                masked_value |= onemask;
                let mut text = format!("{:036b}", masked_value);
                for (idx, chr) in xes.chars().enumerate() {
                    if chr == 'X' {
                        // problem here, verify after mask
                        text = set_char_at(&text, idx, 'X');
                    }
                }
                let xcount : u32 = text.chars().filter(|&c| c == 'X').count() as u32;
                let variants = 2u32.pow(xcount);
                let mut retr = Vec::new();
                for v in 0..variants {
                    let mut vbin = format!("{:b}", v);
                    while vbin.len() < xcount as usize {
                        vbin.insert(0, '0');
                    }
                    let mut vtext = text.clone();
                    let mut xcount = 0;
                    for (idx, chr) in text.chars().enumerate() {
                        if chr == 'X' {
                            vtext = set_char_at(&vtext, idx, vbin.chars().nth(xcount).unwrap());
                            xcount += 1;
                        }
                    }
                    let vval = u64::from_str_radix(&vtext, 2).unwrap();
                    retr.push(vval);
                }
                retr
            }
            Opv2::Write { address: _, value: _} => panic!("apply on a value {:?}", self),
        }
    }
    fn parse(text : &str) -> Opv2 {
        lazy_static! {
            static ref MASK_RX : Regex = Regex::new(r"mask = (.+)$").unwrap();
            static ref WRITE_RX : Regex = Regex::new(r"mem\[(\d+)\] = (\d+)$").unwrap();
        }
        if let Some(captures) = MASK_RX.captures(text) {
            let mask = captures.get(1).unwrap().as_str();
            let mut onemask = 0;
            for (idx, chr) in mask.chars().rev().enumerate() {
                if chr == 'X' {
                    continue;
                } else if chr == '1' {
                    onemask |= 1 << idx;
                }
            }
            Opv2::Mask { onemask, xes : mask.to_string() }
        } else if let Some(captures) = WRITE_RX.captures(text) {
            let address = captures.get(1).unwrap().as_str().parse::<usize>().unwrap();
            let value = captures.get(2).unwrap().as_str().parse::<u64>().unwrap();
            Opv2::Write {
                address,
                value
            }
        } else {
            panic!("can't parse {:?}", text);
        }
    }
}

impl fmt::Debug for Opv2 {
    fn fmt(&self, f : &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Opv2::Mask { onemask, xes } => write!(f, "mask: onemask {:b} xes {:?}", onemask, xes),
            Opv2::Write { address, value } => write!(f, "write[{}] = {}", address, value),
        }
    }
}

fn part2(contents : &String) {
    let lines = contents.split("\n");
    let ops = lines.map(|l| Opv2::parse(l)).collect::<Vec<Opv2>>();
    for op in &ops {
        println!("{:?}", op);
    }
    let mut current_mask = Opv2::empty_mask();
    let mut memory = HashMap::new();
    for op in ops {
        match op {
            Opv2::Mask { onemask: _, xes: _ } => current_mask = op,
            Opv2::Write { address, value } => {
                let addresses = current_mask.apply(address as u64);
                for addr in addresses {
                    memory.insert(addr, value);
                }
            }
        }
    }
    let nonzero : u64 = memory.into_iter().filter(|&v| v.1 != 0).map(|(_, value)| value).sum();
    println!("nonzero {:?}", nonzero);
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    part2(&contents);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_replace_1() {
        let text = "a".to_string();
        assert_eq!(set_char_at(&text, 0, 'b'), "b");
    }

    #[test]
    fn test_replace_3() {
        let text = "abc".to_string();
        assert_eq!(set_char_at(&text, 1, 'c'), "acc");
        assert_eq!(set_char_at(&text, 2, 'd'), "abd");
    }

    #[test]
    fn test_replace_xes() {
        let text = "00000000000000000000000000000111010".to_string();
        assert_eq!(set_char_at(&text, 29, 'X'), "00000000000000000000000000000X11010");
    }


}