use std::fs::File;
use std::io::prelude::*;

// get (1 counts, 3 counts) tuple
fn get_counts(numbers : &Vec<u32>) -> (u32, u32, u32) {
    let mut ones = 0;
    let mut twos = 0;
    let mut threes = 0;
    if numbers[0] == 1 {
        ones += 1;
    } else if numbers[0] == 2 {
        twos += 1;
    } else if numbers[0] == 3 {
        threes += 1;
    }
    for idx in 1..numbers.len() {
        if numbers[idx] - numbers[idx-1] == 1 {
            ones += 1;
        } else if numbers[idx] - numbers[idx-1] == 2 {
            twos += 1;
        } else if numbers[idx] - numbers[idx-1] == 3 {
            threes += 1;
        }
    }
    threes += 1; //end is 3 higher
    (ones, twos, threes)
}

fn is_valid(sequence : &Vec<u32>, first : u32, last: u32) -> bool {
    if sequence.len() < 1 {
        return false;
    }
    if sequence[0] as i32 - first as i32 > 3 {
        return false; //0 - first is too large
    }
    if last as i32 - sequence[sequence.len()-1] as i32 > 3 {
        return false; //last - last +3 > 3
    }
    if sequence.len() < 1 { //removed all except starting 0 and ending last+3 node
        return false;
    }
    for idx in 1..sequence.len() {
        if sequence[idx] as i32 - sequence[idx-1] as i32 > 3 {
            return false;
        }
    }
    true
}

fn split_by_three(numbers : &Vec<u32>) -> Vec<Vec<u32>> {
    let mut fullsequence = numbers.clone();
    fullsequence.insert(numbers.len(), numbers[numbers.len()-1]+3);
    let mut retr : Vec<Vec<u32>> = Vec::new();
    let mut currstart = 0;
    for idx in 1..fullsequence.len() {
        if fullsequence[idx] - fullsequence[idx-1] == 3 {
            let mut sequence = Vec::new();
            for i2 in currstart..idx {
                sequence.push(fullsequence[i2]);
            }
            retr.push(sequence);
            currstart = idx;
        }
    }
    retr
}

fn get_combinations(numbers : &Vec<u32>, first_flexible : bool) -> u32 {
    let mut first = numbers[0];
    if first < 3 && first_flexible {
        first = 0;
    } else {
        first = first-3;
    }
    let last = numbers[numbers.len()-1] + 3;
    let mut count = 1;
    let mut to_try = vec![(0, numbers.clone())]; //0 first, need to keep that
    while !to_try.is_empty() {
        let (curr_idx, curr_numbers) = to_try.pop().unwrap();
        //drop current number
        let mut drop_current = curr_numbers.clone();
        drop_current.remove(curr_idx);
        if is_valid(&drop_current, first, last) {
            to_try.push((curr_idx, drop_current));
            count += 1;
        }
        if curr_idx >= curr_numbers.len()-1 {
            continue;
        } else {
            to_try.push((curr_idx+1, curr_numbers));
        }
    }
    count
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines = contents.split("\n");
    let mut numbers : Vec<u32> = lines.map(|l| l.parse().unwrap()).collect::<Vec<u32>>();
    numbers.sort();

    //part 1
    let (ones, _, threes) = get_counts(&numbers);
    println!("ones {} threes {}, result = {}", ones, threes, ones * threes);

    //part 2
    let parts = split_by_three(&numbers);
    let mut total : u64 = 1;
    println!("{:?}", parts);
    for (idx,p) in parts.iter().enumerate() {
        if p.len() <= 2 {
            //total += 1;
            continue;
        }
        let pcount = get_combinations(&p, idx==0);
        println!("{:?} = {}", p, pcount);
        total *= pcount as u64;
    }
    println!("total {:?}", total);

    //part 2 LUT version
    let LUT = vec![0, 0, 1, 2, 4, 7, 13, 24];
    total = 1;
    for (idx, p) in parts.iter().enumerate() {
        if p.len() <= 2 {
            continue;
        }
        let mut len = p.len();
        if idx == 0 {
            len +=1;
        }
        total *= LUT[len] as u64;
    }
    println!("lut total {:?}", total);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test3() {
        let v = vec![1,2,3];
        assert_eq!(get_combinations(&v, true), 3);
    }
    #[test]
    fn test4() {
        let v = vec![1,2,3,4];
        assert_eq!(get_combinations(&v, true), 7);
    }
    #[test]
    fn test5() {
        let v = vec![1,2,3,4,5];
        assert_eq!(get_combinations(&v, true), 13);
    }
    #[test]
    fn test_short() {
        let v = vec![1,4,5,6,7,10,11,12,15,16,19];
        assert_eq!(get_combinations(&v, true), 8);
    }
}