use std::fs::File;
use std::io::prelude::*;

type Map = Vec<Vec<char>>;

fn print_map(map : &Map) {
    for y in 0..map.len() {
        for x in 0..map[y].len() {
            print!("{}", map[y][x]);
        }
        println!("");
    }
}

fn get_occupied_count(map : &Map, col: usize, row: usize) -> usize {
    let mut retr = 0;
    let colcount = map[0].len();
    let rowcount = map.len();

    //println!("checking col={}, row={}, colcount={}, rowcount={}", col, row, colcount, rowcount);
    // left of center
    if col > 0 && map[row][col-1] == '#' {
        //println!("left of center");
        retr += 1;
    }

    // left and above of center
    if col > 0 && row > 0 && map[row-1][col-1] == '#' {
        //println!("left and above of center");
        retr += 1;
    }

    // left and below of center
    if col > 0 && row < rowcount-1 && map[row+1][col-1] == '#' {
        //println!("left and below of center");
        retr += 1;
    }

    // right of center
    if col < colcount-1 && map[row][col+1] == '#' {
        //println!("right of center");
        retr += 1;
    }

    // right and above of center
    if col < colcount-1 && row > 0 && map[row-1][col+1] == '#' {
        //println!("right and above of center");
        retr += 1;
    }

    // right and below of center
    if col < colcount-1 && row < rowcount -1 && map[row+1][col+1] == '#' {
        //println!("right and below of center");
        retr += 1;
    }

    // above center
    if row > 0 && map[row-1][col] == '#' {
        //println!("above center");
        retr += 1;
    }

    // below center
    if row < rowcount-1 && map[row+1][col] == '#' {
        //println!("below center");
        retr += 1;
    }

    retr
}

fn get_occupied_count_p2(map : &Map, col: usize, row: usize) -> usize {
    let diffs : Vec<(i32, i32)> = vec![(-1,-1),(-1,0),(-1,1),
                                       (0,-1),        (0,1),
                                       (1,-1), (1,0), (1,1)];
    let icol = col as i32;
    let irow = row as i32;
    let mut retr = 0;
    let mut lastpos = (icol, irow);
    let colcount = map[0].len() as i32;
    let rowcount = map.len() as i32;

    for d in diffs {
        loop {
            let (dcol, drow) = (lastpos.0 + d.0, lastpos.1 + d.1);
            if dcol < 0 || dcol >= colcount {
                break;
            }
            if drow < 0 || drow >= rowcount {
                break;
            }
            if map[drow as usize][dcol as usize] == '.' {
                lastpos = (dcol, drow);
                continue;
            } else if map[drow as usize][dcol as usize] == '#' {
                retr += 1;
                break;
            }
            break
        }
        lastpos = (icol, irow);
    }
    retr
}

// If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
fn apply_rule_1(origmap : &Map, map : &mut Map, col : usize, row : usize) {
    if origmap[row][col] != 'L' {
        return;
    }
    if get_occupied_count_p2(origmap, col, row) == 0 {
        map[row][col] = '#';
    }
}

// If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
fn apply_rule_2(origmap : &Map, map : &mut Map, col : usize, row : usize) {
    if origmap[row][col] != '#' {
        return;
    }
    if get_occupied_count_p2(origmap, col, row) >= 5 {
        map[row][col] = 'L';
    }
}

fn parse_map(text : &String) -> Map {
    let lines = text.split("\n");
    let map : Map = lines.map(|line| line.chars().collect::<Vec<char>>()).collect::<Map>();
    map
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut map = parse_map(&contents);
    let mut round = 0;
    loop {
        let mut writemap = map.clone();
        for row in 0..map.len() {
            for col in 0..map[0].len() {
                apply_rule_1(&map, &mut writemap, col, row);
                apply_rule_2(&map, &mut writemap, col, row);
            }
        }
        if map == writemap {
            // no changes
            break;
        }
        round += 1;
        map = writemap.clone();
    }
    let seat_count : usize = map.iter().map(|row| row.iter().filter(|c| **c == '#').count()).sum();
    println!("after {} rounds we have {}", round, seat_count);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_empty_ocupied() {
        let text = ".L.".to_string();
        let map = parse_map(&text);
        assert_eq!(get_occupied_count(&map, 0, 0), 0);
        assert_eq!(get_occupied_count(&map, 1, 0), 0);
        assert_eq!(get_occupied_count(&map, 2, 0), 0);
    }

    #[test]
    fn test_one_ocupied() {
        let text = ".#.".to_string();
        let map = parse_map(&text);
        assert_eq!(get_occupied_count(&map, 0, 0), 1);
        assert_eq!(get_occupied_count(&map, 1, 0), 0);
        assert_eq!(get_occupied_count(&map, 2, 0), 1);
    }


    #[test]
    fn test_nine_ocupied() {
        let text = "###\n###\n###".to_string();
        let map = parse_map(&text);
        // left
        assert_eq!(get_occupied_count(&map, 0, 0), 3);
        assert_eq!(get_occupied_count(&map, 1, 0), 5);
        assert_eq!(get_occupied_count(&map, 2, 0), 3);
        // middle
        assert_eq!(get_occupied_count(&map, 0, 1), 5);
        assert_eq!(get_occupied_count(&map, 1, 1), 8);
        assert_eq!(get_occupied_count(&map, 2, 1), 5);
        // right
        assert_eq!(get_occupied_count(&map, 0, 2), 3);
        assert_eq!(get_occupied_count(&map, 1, 2), 5);
        assert_eq!(get_occupied_count(&map, 2, 2), 3);
    }

    #[test]
    fn test_toprow_ocupied() {
        let text = ".##.\n####\n.#.#".to_string();
        let map = parse_map(&text);

        assert_eq!(get_occupied_count(&map, 0, 0), 3);
        assert_eq!(get_occupied_count(&map, 1, 0), 4);
        assert_eq!(get_occupied_count(&map, 2, 0), 4);
        assert_eq!(get_occupied_count(&map, 3, 0), 3);
    }

    #[test]
    fn test_raycasting_row() {
        let text = "L#.L".to_string();
        let map = parse_map(&text);
        assert_eq!(get_occupied_count_p2(&map, 0, 0), 1);
        assert_eq!(get_occupied_count_p2(&map, 1, 0), 0);
        assert_eq!(get_occupied_count_p2(&map, 2, 0), 1);
        assert_eq!(get_occupied_count_p2(&map, 3, 0), 1);
    }

    #[test]
    fn test_raycasting_col() {
        let text = "L\n#\n.\nL".to_string();
        let map = parse_map(&text);
        assert_eq!(get_occupied_count_p2(&map, 0, 0), 1);
        assert_eq!(get_occupied_count_p2(&map, 0, 1), 0);
        assert_eq!(get_occupied_count_p2(&map, 0, 2), 1);
        assert_eq!(get_occupied_count_p2(&map, 0, 3), 1);
    }

    #[test]
    fn test_raycasting_diagonal() {
        let text = "L#.L\n#L#L".to_string();
        let map = parse_map(&text);
        assert_eq!(get_occupied_count_p2(&map, 0, 0), 2);
        assert_eq!(get_occupied_count_p2(&map, 1, 0), 2);
        assert_eq!(get_occupied_count_p2(&map, 2, 0), 2);
        assert_eq!(get_occupied_count_p2(&map, 3, 0), 2);
    }
}
